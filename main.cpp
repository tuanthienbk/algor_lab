#include <iostream>
#include <mutex>
#include <thread>
#include <chrono>
#include <cassert>

#include "string_algor.h"

void kmp_test()
{
    string sample = "ABC ABCDAB ABCDABCDABDE";
    string pattern = "ABCDABD";
    int rs = kmp(sample.c_str(), sample.size(), pattern.c_str(), pattern.size());
    assert(rs == 15);
}

void suffix_array_test()
{
    string s = "bobocel";
    vector<int> ret = suffix_array(s.c_str(), s.size());
    assert(ret[0]==0 && ret[1]==5 && ret[2]==1 && ret[3]==6 && ret[4] == 2 && ret[5] == 3 && ret[6] == 4);
}

void rms_test()
{
    vector<int> d = {2,4,3,1,9,0,8,6,1,7};
    vector<vector<int>> m = rms_st(d.data(), d.size());
    int r1 = rms_query_st(d.data(),m,0,4);
    assert(r1 == 3);
    int r2 = rms_query_st(d.data(),m,6,9);
    assert(r2 == 8);
    int r3 = rms_query_st(d.data(),m,0,7);
    assert(r3 == 5);
}

void pm1_rmq_test()
{
    vector<int> d = {0,1,2,3,4,5,4,3,4,5,
                     6,7,8,7,6,5,6,5,4,3};
    LCA_RMQ rmq(d.data(),d.size());
    int r1 = rmq.query(d.data(),0,4);
    assert(r1 == 0);
    int r2 = rmq.query(d.data(),10,19);
    assert(r2 == 19);
    int r3 = rmq.query(d.data(),5,15);
    assert(r3 == 7);
}

void lca_test()
{
    vector<Node*> nodes(13);
    for(int i = 1; i < 14; i++)
        nodes[i-1] = new Node(i);
    nodes[0]->children.push_back(nodes[1]);
    nodes[0]->children.push_back(nodes[2]);
    nodes[0]->children.push_back(nodes[3]);
    nodes[2]->children.push_back(nodes[4]);
    nodes[2]->children.push_back(nodes[5]);
    nodes[2]->children.push_back(nodes[6]);
    nodes[5]->children.push_back(nodes[7]);
    nodes[5]->children.push_back(nodes[8]);
    nodes[6]->children.push_back(nodes[9]);
    nodes[6]->children.push_back(nodes[10]);
    nodes[9]->children.push_back(nodes[11]);
    nodes[9]->children.push_back(nodes[12]);

    LCA lca(nodes[0]);
    Node* pNode = lca.query(nodes[8], nodes[11]);
    assert (pNode == nodes[2]);
}

void fast_rmq_test()
{
    vector<int> d = {2,4,3,1,6,7,8,9,1,7};
    FastRMQ frmq(d.data(), d.size());
    int r1 = frmq.query(0,8);
    assert( r1 == 3);
    int r2 = frmq.query(4,9);
    assert( r2 == 8);
}

void sa_lcp_test()
{
    string s = "bobocel";
    SurffixArray suffix_array(s.c_str(), s.size());
    int r1 = suffix_array.lcp_query(0,2);
    assert(r1 == 2);
    int r2 = suffix_array.lcp_query(1,3);
    assert(r2 == 1);
}

int main(int argc, char *argv[])
{
//    kmp_test();
//    suffix_array_test();
//    rms_test();
//    pm1_rmq_test()
//    lca_test();
//    fast_rmq_test();
    sa_lcp_test();
    return 0;
}
