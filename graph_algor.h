#ifndef GRAPH_ALGOR_H
#define GRAPH_ALGOR_H

#include <string>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

struct Node
{
    int v;
    vector<Node*> children;

    Node(int i, bool isBinary = false) : v(i)
    {
        if (isBinary)
            children.resize(2, NULL);
    }

};

#endif // GRAPH_ALGOR_H
