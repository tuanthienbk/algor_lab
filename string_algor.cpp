#include "string_algor.h"

// Knuth-Morris-Pratt
int kmp(const char* str, int n /*length of str*/, const char* pattern, int m /*length of pattern*/)
{
    // compute pi for longest prefix which is suffix of P[1..i]
    vector<int> pi(m+1);
    pi[0] = -1;
    for(int i = 1; i <=m; i++)
    {
        pi[i] = pi[i-1];
        while(pi[i] >= 0 && pattern[pi[i]] != pattern[i-1])
            pi[i] = pi[pi[i]];
        pi[i] += 1;
    }
    // matching
    for(int i = 0; i < n;/*nothing*/)
    {
        int j;
        for(j = 0; j < m; j++)
            if (str[i+j] != pattern[j])
                break;
        if (j == m)
            return i;
        else
            i += j - pi[j];
    }
    return -1;
}

vector<int> suffix_array(const char* str, int n)
{
    vector<vector<int>> P;
    vector<pair<pair<int,int>,int>> L(n);
    const int SMALL_INT = -1;

    P.push_back(vector<int>(n,0));
    for(int i = 0; i < n; i++)
        P[0][i] = str[i] - 'a';

    for(int k = 1, cnt = 1; cnt < n; cnt <<= 1, k++)
    {
        for(int i = 0; i < n; i++)
            L[i] = make_pair(make_pair(P[k-1][i], (i+cnt < n)?P[k-1][i+cnt]:SMALL_INT), i);

        sort(L.begin(), L.end());

        P.push_back(vector<int>(n,0));
        for(int i = 0; i < n; i++)
            P[k][L[i].second] = ( i > 0 && L[i].first == L[i-1].first ) ? P[k][L[i-1].second]:i;
    }

    return P.back();
}

int log2(int N)
{
    int ret = 0;
    while ( N >>= 1)
        ret++;
    return ret;
}

vector<vector<int>> rms_st(int* A, int N)
{
    int Log2N = log2(N)+1;

    vector<vector<int>> M(N, vector<int>(Log2N));
    // initialize for length 1
    for(int i =  0; i < N; i++)
        M[i][0] = i;
    // dynamic programming
    for(int j = 1; j < Log2N; j++)
    {
        int k = 1<<(j-1);
        for(int i = 0; i < N; i++)
        {
            if (i + k >= N)
                M[i][j] = M[i][j-1];
            else
                M[i][j] = (A[M[i][j-1]] <= A[M[i + k][j-1]]) ? M[i][j-1]:M[i + k][j-1];
        }
    }

    return M;
}

int rms_query_st(int* A, const vector<vector<int>> M,
                 int low_idx, int high_idx)
{
    if (high_idx < low_idx) return -1;

    int k = log2(high_idx - low_idx + 1);

    return (A[M[low_idx][k]] <= A[M[high_idx-(1<<k)+1][k]]) ? M[low_idx][k] : M[high_idx-(1<<k)+1][k];
}

void gen_block_sparse_table(int s, vector<INT_MATRIX>& ret,
                            map<vector<int>,int>& vec_map)
{
    int m = 1<<(s-1);
    ret.resize(m);
    vector<vector<int>> d(m, vector<int>(s));
    d[0][0] = 0;
    for(int i = 0; i < s-1; i++)
    {
        int k = 1<<i;
        for(int j = 0;j < k; j++)
        {
            d[j+k] = d[j];
            d[j][i+1] = d[j][i] + 1;
            d[j+k][i+1] = d[j][i] - 1;
        }
    }

    for(int i = 0; i < m; i++)
    {
        ret[i] = rms_st(d[i].data(), d[i].size());
        vec_map[d[i]] = i;
    }
}

LCA_RMQ::LCA_RMQ(int* A, int n)
{
    N = n;
    int Log2n = log2(n);
    s = (Log2n+1)/2;

    map<vector<int>,int> vec_map;
    gen_block_sparse_table(s, BST, vec_map);

    int S = (N + s - 1)/s;
    Ap.resize(S);
    B.resize(S,0);
    BST_Lookup.resize(S);
    for(int i = 0; i < S; i++)
    {
        int k = i*s;
        Ap[i] = A[k];
        vector<int> temp_vec;
        temp_vec.reserve(s);
        temp_vec.push_back(0);
        for(int j = 1; j < s && k + j < N; j++)
        {
            temp_vec.push_back(A[k+j]-A[k]);
            if (A[k+j] < Ap[i])
            {
                Ap[i] = A[k+j];
                B[i] = j;
            }
        }
        if (temp_vec.size() == s)
        {
            BST_Lookup[i] = vec_map[temp_vec];
        }
        else
        {
            BST_Lookup[i] = i;
            BST.push_back(rms_st(temp_vec.data(), temp_vec.size()));
        }
    }
    M = rms_st(Ap.data(), Ap.size());
}

int LCA_RMQ::query(int* A, int i, int j)
{
    if (i > j) return -1;
    int block_i = i/s;
    int block_j = j/s;
    int iblock = i - block_i*s;
    int jblock = j - block_j*s;
    if ( block_i == block_j)
    {
        return block_i*s + rms_query_st(&A[block_i*s], BST[BST_Lookup[block_i]], iblock, jblock);
    }
    else {
        int a = rms_query_st(&A[block_i*s], BST[BST_Lookup[block_i]], iblock, s);
        int b = rms_query_st(&A[block_j*s], BST[BST_Lookup[block_j]], 0, jblock);
        int c = (A[block_i*s + a] < A[block_j*s + b]) ? block_i*s + a : block_j*s + b;
        int d = rms_query_st(Ap.data(), M, block_i+1, block_j-1);
        if (d < 0)
            return c;
        else
            return (A[c] < A[d*s + B[d]])? c: d*s + B[d];
    }
}


void LCA::gen_lca_structure(Node* root, int level, vector<Node*>& E, vector<int>& L, map<Node*,int>& H)
{
    if (!root) return;

    H[root] = E.size();
    E.push_back(root);
    L.push_back(level);

    if ( !root->children.empty())
    {
        for(auto c: root->children)
        {
            gen_lca_structure(c,level+1,E,L,H);
            E.push_back(root);
            L.push_back(level);
        }
    }
}

LCA::LCA(Node *r) : root(r)
{
    gen_lca_structure(root, 0, E, L, H);
    rmq = new LCA_RMQ(L.data(), L.size());
}

Node *LCA::query(Node *a, Node *b)
{
    return E[rmq->query(L.data(),H[a], H[b])];
}

void iLCA::gen_lca_structure(Node* root, int level, vector<int>& E, vector<int>& L, map<int,int>& H)
{
    if (!root) return;

    H[root->v] = E.size();
    E.push_back(root->v);
    L.push_back(level);

    if ( !root->children.empty())
    {
        for(auto c: root->children)
        {
            gen_lca_structure(c,level+1,E,L,H);
            E.push_back(root->v);
            L.push_back(level);
        }
    }
}

iLCA::iLCA(Node *r) : root(r)
{
    gen_lca_structure(root, 0, E, L, H);
    rmq = new LCA_RMQ(L.data(), L.size());
}

int iLCA::query(int i, int j)
{
    return E[rmq->query(L.data(),H[i], H[j])];
}

FastRMQ::FastRMQ(int *A, int Size)
{
    N = Size;
    gen_cartesian_tree(A, N);
    lca = new iLCA(root);
}

int FastRMQ::query(int i, int j)
{
    return lca->query(i,j);
}

void FastRMQ::gen_cartesian_tree(int *A, int N)
{
    vector<Node*> st; // stack
    for(int i = 0; i < N; i++)
    {
        Node* n = new Node(i, true);
        Node* prev = NULL;
        while (!st.empty() && A[st.back()->v] > A[n->v])
        {
            prev = st.back();
            st.pop_back();
        }
        if (!st.empty())
            st.back()->children[1] = n;
        if (prev)
            n->children[0] = prev;
        st.push_back(n);
    }
    root = st.front();
}

SurffixArray::SurffixArray(const char *str, int n)
{
    vector<vector<int>> P;
    vector<pair<pair<int,int>,int>> L(n);
    const int SMALL_INT = -1;

    P.push_back(vector<int>(n,0));
    for(int i = 0; i < n; i++)
        P[0][i] = str[i] - 'a';

    for(int k = 1, cnt = 1; cnt < n; cnt <<= 1, k++)
    {
        for(int i = 0; i < n; i++)
            L[i] = make_pair(make_pair(P[k-1][i], (i+cnt < n)?P[k-1][i+cnt]:SMALL_INT), i);

        sort(L.begin(), L.end());

        P.push_back(vector<int>(n,0));
        for(int i = 0; i < n; i++)
            P[k][L[i].second] = ( i > 0 && L[i].first == L[i-1].first ) ? P[k][L[i-1].second]:i;
    }
    sa = P.back();
    vector<int> isa(n);
    for(int i = 0; i < n; i++)
        isa[sa[i]] = i;

    lca.resize(n-1);
    for(int d = 0; d < n-1; d++)
    {
        int len = 0;
        int i = isa[d];
        int j = isa[d+1];
        for (int k = P.size() - 1; k >= 0 && i < n && j < n; k--) {
            if (P[k][i] == P[k][j]) {
                i += 1 << k;
                j += 1 << k;
                len += 1 << k;
            }
        }
        lca[d] = len;
    }

    sa_rmq = new FastRMQ(lca.data(), lca.size());

}

int SurffixArray::lcp_query(int x, int y)
{
    if (x==y)
    {
        return sa.size() - x;
    }
    else
    {
        int& i = sa[x];
        int& j = sa[y];
        return (i < j) ? lca[sa_rmq->query(i, j-1)] : lca[sa_rmq->query(j, i-1)];
    }
}
