#ifndef STRING_ALGOR_H
#define STRING_ALGOR_H

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <utility>

#include "graph_algor.h"

using namespace std;

// Knuth-Morris-Pratt
int kmp(const char* str, int n /*length of str*/, const char* pattern, int m /*length of pattern*/);

// suffix array
vector<int> suffix_array(const char* /*string*/, int /*length of str*/);

// RMQ -- Range Minimal Query -- Implementation of "The LCA Problem Revisited"
// Sparse Table < O(nlogn), O(1)>
int log2(int N);
vector<vector<int>> rms_st(int* /*input array*/, int /*size*/);
int rms_query_st(int* /*input array*/, const vector<vector<int>> /*data_structure*/,
              int low_idx, int high_idx);
// Special case +-1 RMQ
#define INT_MATRIX vector<vector<int>>
// the size of the returning structure = sqrt(n) * log(n)/2 * log(log(n)/2)
void gen_block_sparse_table(int /*size of array*/, vector<INT_MATRIX>& /*resulting struct*/,
                            map<vector<int>,int>& /*map a vector to an element of the struct*/);

class LCA_RMQ{
private:
    vector<int> Ap;
    vector<int> B;
    INT_MATRIX M;
    vector<INT_MATRIX> BST; // block sparse table
    vector<int> BST_Lookup;

    int N; // size of array
    int s; // size of block
public:
    LCA_RMQ(int* /*input array*/, int /*size*/);
    int query(int* /*input array*/, int /*low index*/, int /*high index*/);
};

class LCA
{
private:
    Node* root;
    vector<Node*> E;
    vector<int> L;
    map<Node*,int> H;
    LCA_RMQ* rmq;
public:
    LCA(Node* r);
    Node* query(Node* a, Node* b);
private:
    void gen_lca_structure(Node* root, int level, vector<Node*>& E, vector<int>& L, map<Node*,int>& H);
};

class iLCA
{
private:
    Node* root;
    vector<int> E;
    vector<int> L;
    map<int,int> H;
    LCA_RMQ* rmq;
public:
    iLCA(Node* r);
    int query(int i, int j);
private:
    void gen_lca_structure(Node* root, int level, vector<int>& E, vector<int>& L, map<int,int>& H);
};

class FastRMQ
{
private:
    int N;
    Node* root;
    iLCA* lca;
public:
    FastRMQ(int* /*input array*/, int /*size*/);
    int query(int /*low index*/, int /*high index*/);
private:
    void gen_cartesian_tree(int* /*input array*/, int /*size*/);
};

class SurffixArray
{
private:
    FastRMQ* sa_rmq;
    vector<int> sa;
    vector<int> lca;
public:
    SurffixArray (const char* /*string*/, int /*length of str*/);
    int lcp_query(int x, int y);
};



#endif // STRING_ALGOR_H
